<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">


    <!-- Font Icon -->
    <link rel="stylesheet" href="fonts/material-icon/css/material-design-iconic-font.min.css">

    <!-- Main css -->

    <link rel="stylesheet" href="js/bootstrap.min.js" />
    <link rel="stylesheet" href="css/customerRegistraion.css" />

    <!-- image upload -->
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/croppie.js"></script>
    <link rel="stylesheet" href="css/bootstrap.min.css" />
    <link rel="stylesheet" href="css/croppie.css" />

    <style>
    body {
        background-image: url("img/customerRegistraion/signup-bg.jpg");
    }

    .img-thumbnail {
        border-radius: 50%;
    }

    .signup-content {
        max-width: 800px;
        margin: auto;
        margin-left: auto;
        margin-right: auto;
    }

    .img {
        width: 200px;
        height: 200px;
        border-radius: 120px;
        margin: 10px;
        display: block;
        margin-left: auto;
        margin-right: auto;
    }

    .fake-btn {
        flex-shrink: 0;
        border: none;
        border-radius: 3px;
        padding: 8px 15px;
        margin-right: 10px;
        font-size: 12px;
        text-transform: uppercase;
        cursor: pointer;
        color: #fff;
        background-image: linear-gradient(to right, #a1c4fd 0%, #c2e9fb 51%, #a1c4fd 100%);
    }

    .inputfile {
        margin-top: -22px;
        margin-left: auto;
        margin-right: auto;
        left: auto;
        top: auto;
        height: 12%;
        width: 12%;
        cursor: pointer;
        opacity: 0;

        &:focus {
            outline: none;
        }
    }
    </style>
</head>

<body>

<?php

$imagePath = "img/customerRegistraion/user2.png";
if (! empty($uploadedImagePath)) {
    $imagePath = $uploadedImagePath;
}

?>


    <div class="main">
        <section class="signup">
            <div class="container">
                <div class="signup-content">
                    <form method="POST" id="signup-form" class="signup-form">
                        <div class="panel-body" align="center">
                            <div id="uploaded_image">

                                <input type="image" class="img" src="<?php echo $imagePath; ?>" />
                            </div>
                            <br>
                            <span class="fake-btn">Choose files</span>
                            <br>
                            <input type="file" name="upload_image" id="upload_image" class="inputfile" />
                            <br />


                            <div class="form-group">
                                <input type="text" class="form-input" name="name" id="name" placeholder="First Name" />
                                <br>
                                <br>
                                <input type="text" class="form-input" name="name" id="name" placeholder="Last Name" />
                            </div>

                            <div class="form-group">
                                <input type="number" class="form-input" name="number" id="number"
                                    placeholder="Your Mobile Number" />
                                <br>
                                <br>
                                <input type="email" class="form-input" name="email" id="email"
                                    placeholder="Your Email" />
                            </div>

                            <div class="form-group">
                                <input type="password" class="form-input" name="password" id="password"
                                    placeholder="Password" />
                                <span toggle="#password" onclick="showPassword()" class="zmdi zmdi-eye field-icon toggle-password"></span>
                            </div>

                            <div class="form-group">
                                <input type="password" class="form-input" name="re_password" id="re_password"
                                    placeholder="Repeat your password" />
                            </div>
                            
                            <div class="form-group">
                                <input type="checkbox" name="agree-term" id="agree-term" class="agree-term" />
                                <label for="agree-term" class="label-agree-term"><span><span></span></span>I agree all
                                    statements in <a href="#" class="term-service">Terms of service</a></label>
                            </div>
                            <div class="form-group">
                                <input type="submit" name="submit" id="submit" class="form-submit" value="Sign up" />
                            </div>
                    </form>
                    <p class="loginhere">
                        Have already an account ? <a href="my-account.php" class="loginhere-link">Login here</a>
                    </p>
                </div>
            </div>


    </div>
    </div>




    </div>
    </section>

    </div>





    <div id="uploadimageModal" class="modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Upload & Crop Image</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-8 text-center">
                            <div id="image_demo" style="width:350px; margin-top:30px"></div>
                        </div>
                        <div class="col-md-4" style="padding-top:30px;">
                            <br />
                            <br />
                            <br />
                            <button class="btn btn-success crop_image">Crop & Upload Image</button>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

  


<script>

function showPassword() {
  var x = document.getElementById("password");
  if (x.type === "password") {
    x.type = "text";
  } else {
    x.type = "password";
  }
}


    $(document).ready(function() {

        $image_crop = $('#image_demo').croppie({
            enableExif: true,
            viewport: {
                width: 200,
                height: 200,
                type: 'circle' //square
            },
            boundary: {
                width: 300,
                height: 300
            }
        });

        $('#upload_image').on('change', function() {
            var reader = new FileReader();
            reader.onload = function(event) {
                $image_crop.croppie('bind', {
                    url: event.target.result
                }).then(function() {
                    console.log('jQuery bind complete');
                });
            }
            reader.readAsDataURL(this.files[0]);
            $('#uploadimageModal').modal('show');
        });

        $('.crop_image').click(function(event) {
            $image_crop.croppie('result', {
                type: 'canvas',
                size: 'viewport'
            }).then(function(response) {
                $.ajax({
                    url: "upload.php",
                    type: "POST",
                    data: {
                        "image": response
                    },
                    success: function(data) {
                        $('#uploadimageModal').modal('hide');
                        $('#uploaded_image').html(data);
                    }
                });
            })
        });

    });
    </script>
</body>

</html>