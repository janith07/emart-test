
        <div class="header__top">
            <div class="container">
                <div class="header__left">
                    <p>Welcome to Martfury Online Shopping Store !</p>
                </div>
                <div class="header__right">
                    <ul class="header__top-links">
                        <li><a href="#">Store Location</a></li>
                        <li><a href="#">Track Your Order</a></li>
                        <li>
                            <div class="ps-dropdown"><a href="#">US Dollar</a>
                                <ul class="ps-dropdown-menu">
                                    <li><a href="#">Us Dollar</a></li>
                                    <li><a href="#">Euro</a></li>
                                </ul>
                            </div>
                        </li>
                        <li>
                            <div class="ps-dropdown language"><a href="#"><img src="img/flag/en.png" alt="">English</a>
                                <ul class="ps-dropdown-menu">
                                    <li><a href="#"><img src="img/flag/germany.png" alt=""> Germany</a></li>
                                    <li><a href="#"><img src="img/flag/fr.png" alt=""> France</a></li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="header__content">
            <div class="container">
                <div class="header__content-left"><a class="ps-logo" href="index.php"><img src="img/logo.png" alt=""></a>
                    <div class="menu--product-categories">
                        <div class="menu__toggle"><i class="icon-menu"></i><span> Shop by Department</span></div>
                        <div class="menu__content">
                            <ul class="menu--dropdown">
                                <li><a href="#"><i class="icon-star"></i> Hot Promotions</a>
                                </li>
                                <li class="menu-item-has-children has-mega-menu"><a href="#"><i class="icon-laundry"></i> Consumer Electronic</a>
                                    <div class="mega-menu">
                                        <div class="mega-menu__column">
                                            <h4>Electronic<span class="sub-toggle"></span></h4>
                                            <ul class="mega-menu__list">
                                                <li><a href="#">Home Audio &amp; Theathers</a>
                                                </li>
                                                <li><a href="#">TV &amp; Videos</a>
                                                </li>
                                                <li><a href="#">Camera, Photos &amp; Videos</a>
                                                </li>
                                                <li><a href="#">Cellphones &amp; Accessories</a>
                                                </li>
                                                <li><a href="#">Headphones</a>
                                                </li>
                                                <li><a href="#">Videosgames</a>
                                                </li>
                                                <li><a href="#">Wireless Speakers</a>
                                                </li>
                                                <li><a href="#">Office Electronic</a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="mega-menu__column">
                                            <h4>Accessories &amp; Parts<span class="sub-toggle"></span></h4>
                                            <ul class="mega-menu__list">
                                                <li><a href="#">Digital Cables</a>
                                                </li>
                                                <li><a href="#">Audio &amp; Video Cables</a>
                                                </li>
                                                <li><a href="#">Batteries</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </li>
                                <li><a href="#"><i class="icon-shirt"></i> Clothing &amp; Apparel</a>
                                </li>
                                <li><a href="#"><i class="icon-lampshade"></i> Home, Garden &amp; Kitchen</a>
                                </li>
                                <li><a href="#"><i class="icon-heart-pulse"></i> Health &amp; Beauty</a>
                                </li>
                                <li><a href="#"><i class="icon-diamond2"></i> Yewelry &amp; Watches</a>
                                </li>
                                <li class="menu-item-has-children has-mega-menu"><a href="#"><i class="icon-desktop"></i> Computer &amp; Technology</a>
                                    <div class="mega-menu">
                                        <div class="mega-menu__column">
                                            <h4>Computer &amp; Technologies<span class="sub-toggle"></span></h4>
                                            <ul class="mega-menu__list">
                                                <li><a href="#">Computer &amp; Tablets</a>
                                                </li>
                                                <li><a href="#">Laptop</a>
                                                </li>
                                                <li><a href="#">Monitors</a>
                                                </li>
                                                <li><a href="#">Networking</a>
                                                </li>
                                                <li><a href="#">Drive &amp; Storages</a>
                                                </li>
                                                <li><a href="#">Computer Components</a>
                                                </li>
                                                <li><a href="#">Security &amp; Protection</a>
                                                </li>
                                                <li><a href="#">Gaming Laptop</a>
                                                </li>
                                                <li><a href="#">Accessories</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </li>
                                <li><a href="#"><i class="icon-baby-bottle"></i> Babies &amp; Moms</a>
                                </li>
                                <li><a href="#"><i class="icon-baseball"></i> Sport &amp; Outdoor</a>
                                </li>
                                <li><a href="#"><i class="icon-smartphone"></i> Phones &amp; Accessories</a>
                                </li>
                                <li><a href="#"><i class="icon-book2"></i> Books &amp; Office</a>
                                </li>
                                <li><a href="#"><i class="icon-car-siren"></i> Cars &amp; Motocycles</a>
                                </li>
                                <li><a href="#"><i class="icon-wrench"></i> Home Improments</a>
                                </li>
                                <li><a href="#"><i class="icon-tag"></i> Vouchers &amp; Services</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="header__content-center">
                    <form class="ps-form--quick-search" action="index.php" method="get">
                        <div class="form-group--icon"><i class="icon-chevron-down"></i>
                            <select class="form-control">
                                <option value="1">All</option>
                                <option value="1">Smartphone</option>
                                <option value="1">Sounds</option>
                                <option value="1">Technology toys</option>
                            </select>
                        </div>
                        <input class="form-control" type="text" placeholder="I'm shopping for...">
                        <button>Search</button>
                    </form>
                    <p><a href="#">iphone x</a><a href="#">virtual</a><a href="#">apple</a><a href="#">wireless</a><a href="#">simple chair</a><a href="#">classic watch</a><a href="#">macbook</a></p>
                </div>


               

                <div class="header__content-right">
                    <div class="header__actions">
                  
                        <!-- icon user -->

                        <div class="ps-cart--mini"><a class="header__extra" href="#">
                        <i class="icon-user"></i>
                        <span><i>0</i></span>
                        </a>
                            <div class="ps-cart__content">
                                <div class="ps-cart__items">
                                    
                                <div class="avatar">   
                                <img src="img/avatar1.png" alt="avatar" style="width:30%; border-radius: 50%; margin-left:100px;">
                                </div>

                                <form>
                                <label for="uname"><b>Username</b></label>
                                <input class="data" type="text" placeholder="Enter Username" name="uname" required>

                                <label for="psw"><b>Password</b></label>
                                <input class="data" type="password" placeholder="Enter Password" name="psw" required>
                                    
                                <a href="customer-dashboard.php" class="btnlogin" type="submit">Login</a>
                                <label>
                                    <input class="data" type="checkbox" checked="checked" name="remember"> Remember me
                                </label>
                                </form>

                            </div>
                                   
                               
                                <div class="ps-cart__footer">
                                    <!-- <h3>Sub Total:<strong>$59.99</strong></h3> -->
                                    <figure><a class="ps-btn"  style="background-color:green;color:white;" href="shopping-cart.php">cancel</a>
                                     <a style="color:blue;" href="checkout.php"><span style="color:black;">Forgot</span> Password?</a></figure>
                                </div>
                            </div>
                        </div>






                        <div class="ps-block--user-header">
                            <div class="ps-block__left" style="background-color: #fcb800; width:100px; height:40px; border-radius:10px;  padding: 20px; margin: auto;">                      
                            <a href="customerRegistraion.php">Sign Up</a></div>      
                    </div>
                </div>
            </div>
        </div>



        <nav class="navigation">
            <div class="container">
                <ul class="menu menu--market-2">
                    <li><a href="shop-default.php">All Categories</a>
                    </li>
                    <li><a href="shop-default.php">Today Deals</a>
                    </li>
                    <li><a href="shop-default.php">Electronics</a>
                    </li>
                    <li><a href="shop-default.php">Clothing</a>
                    </li>
                    <li><a href="shop-default.php">Computers</a>
                    </li>
                    <li><a href="shop-default.php">Furnitures</a>
                    </li>
                    <li><a href="shop-default.php">Mom &amp; baby</a>
                    </li>
                    <li><a href="shop-default.php">Book &amp; More</a>
                    </li>
                </ul>
            </div>
        </nav>



  