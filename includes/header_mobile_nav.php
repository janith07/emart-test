<div class="ps-panel--sidebar" id="cart-mobile">
        <div class="ps-panel__header">
            <h3>Shopping Cart</h3>
        </div>
        <div class="navigation__content">
            <div class="ps-cart--mobile">
                <div class="ps-cart__content">
                    <div class="ps-product--cart-mobile">
                        <div class="ps-product__thumbnail"><a href="#"><img src="img/products/clothing/7.jpg" alt=""></a></div>
                        <div class="ps-product__content"><a class="ps-product__remove" href="#"><i class="icon-cross"></i></a><a href="product-default.php">MVMTH Classical Leather Watch In Black</a>
                            <p><strong>Sold by:</strong> YOUNG SHOP</p><small>1 x $59.99</small>
                        </div>
                    </div>
                </div>
                <div class="ps-cart__footer">
                    <h3>Sub Total:<strong>$59.99</strong></h3>
                    <figure><a class="ps-btn" href="shopping-cart.php">View Cart</a><a class="ps-btn" href="checkout.php">Checkout</a></figure>
                </div>
            </div>
        </div>
    </div>
    <div class="ps-panel--sidebar" id="navigation-mobile">
        <div class="ps-panel__header">
            <h3>Categories</h3>
        </div>
        <div class="ps-panel__content">
            <ul class="menu--mobile">
                <li><a href="#">Hot Promotions</a>
                </li>
                <li class="menu-item-has-children has-mega-menu"><a href="#">Consumer Electronic</a><span class="sub-toggle"></span>
                    <div class="mega-menu">
                        <div class="mega-menu__column">
                            <h4>Electronic<span class="sub-toggle"></span></h4>
                            <ul class="mega-menu__list">
                                <li><a href="#">Home Audio &amp; Theathers</a>
                                </li>
                                <li><a href="#">TV &amp; Videos</a>
                                </li>
                                <li><a href="#">Camera, Photos &amp; Videos</a>
                                </li>
                                <li><a href="#">Cellphones &amp; Accessories</a>
                                </li>
                                <li><a href="#">Headphones</a>
                                </li>
                                <li><a href="#">Videosgames</a>
                                </li>
                                <li><a href="#">Wireless Speakers</a>
                                </li>
                                <li><a href="#">Office Electronic</a>
                                </li>
                            </ul>
                        </div>
                        <div class="mega-menu__column">
                            <h4>Accessories &amp; Parts<span class="sub-toggle"></span></h4>
                            <ul class="mega-menu__list">
                                <li><a href="#">Digital Cables</a>
                                </li>
                                <li><a href="#">Audio &amp; Video Cables</a>
                                </li>
                                <li><a href="#">Batteries</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </li>
                <li><a href="#">Clothing &amp; Apparel</a>
                </li>
                <li><a href="#">Home, Garden &amp; Kitchen</a>
                </li>
                <li><a href="#">Health &amp; Beauty</a>
                </li>
                <li><a href="#">Yewelry &amp; Watches</a>
                </li>
                <li class="menu-item-has-children has-mega-menu"><a href="#">Computer &amp; Technology</a><span class="sub-toggle"></span>
                    <div class="mega-menu">
                        <div class="mega-menu__column">
                            <h4>Computer &amp; Technologies<span class="sub-toggle"></span></h4>
                            <ul class="mega-menu__list">
                                <li><a href="#">Computer &amp; Tablets</a>
                                </li>
                                <li><a href="#">Laptop</a>
                                </li>
                                <li><a href="#">Monitors</a>
                                </li>
                                <li><a href="#">Networking</a>
                                </li>
                                <li><a href="#">Drive &amp; Storages</a>
                                </li>
                                <li><a href="#">Computer Components</a>
                                </li>
                                <li><a href="#">Security &amp; Protection</a>
                                </li>
                                <li><a href="#">Gaming Laptop</a>
                                </li>
                                <li><a href="#">Accessories</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </li>
                <li><a href="#">Babies &amp; Moms</a>
                </li>
                <li><a href="#">Sport &amp; Outdoor</a>
                </li>
                <li><a href="#">Phones &amp; Accessories</a>
                </li>
                <li><a href="#">Books &amp; Office</a>
                </li>
                <li><a href="#">Cars &amp; Motocycles</a>
                </li>
                <li><a href="#">Home Improments</a>
                </li>
                <li><a href="#">Vouchers &amp; Services</a>
                </li>
            </ul>
        </div>
    </div>
    <div class="navigation--list">
        <div class="navigation__content"><a class="navigation__item ps-toggle--sidebar" href="#menu-mobile"><i class="icon-menu"></i><span> Menu</span></a><a class="navigation__item ps-toggle--sidebar" href="#navigation-mobile"><i class="icon-list4"></i><span> Categories</span></a><a class="navigation__item ps-toggle--sidebar" href="#search-sidebar"><i class="icon-magnifier"></i><span> Search</span></a><a class="navigation__item ps-toggle--sidebar" href="#cart-mobile"><i class="icon-bag2"></i><span> Cart</span></a></div>
    </div>
    <div class="ps-panel--sidebar" id="search-sidebar">
        <div class="ps-panel__header">
            <form class="ps-form--search-mobile" action="index.php" method="get">
                <div class="form-group--nest">
                    <input class="form-control" type="text" placeholder="Search something...">
                    <button><i class="icon-magnifier"></i></button>
                </div>
            </form>
        </div>
        <div class="navigation__content"></div>
    </div>
    <div class="ps-panel--sidebar" id="menu-mobile">
        <div class="ps-panel__header">
            <h3>Menu</h3>
        </div>
        <div class="ps-panel__content">
            <ul class="menu--mobile">
                <li class="current-menu-item menu-item-has-children"><a href="index.php">Home</a><span class="sub-toggle"></span>
                    <ul class="sub-menu">
                        <li><a href="index.php">Marketplace Full Width</a>
                        </li>
                        <li><a href="homepage-2.php">Home Auto Parts</a>
                        </li>
                        <li><a href="homepage-10.php">Home Technology</a>
                        </li>
                        <li><a href="homepage-9.php">Home Organic</a>
                        </li>
                        <li><a href="homepage-3.php">Home Marketplace V1</a>
                        </li>
                        <li><a href="homepage-4.php">Home Marketplace V2</a>
                        </li>
                        <li><a href="homepage-5.php">Home Marketplace V3</a>
                        </li>
                        <li><a href="homepage-6.php">Home Marketplace V4</a>
                        </li>
                        <li><a href="homepage-7.php">Home Electronic</a>
                        </li>
                        <li><a href="homepage-8.php">Home Furniture</a>
                        </li>
                        <li><a href="homepage-kids.php">Home Kids</a>
                        </li>
                        <li><a href="homepage-photo-and-video.php">Home photo and picture</a>
                        </li>
                    </ul>
                </li>
                <li class="menu-item-has-children has-mega-menu"><a href="shop-default.php">Shop</a><span class="sub-toggle"></span>
                    <div class="mega-menu">
                        <div class="mega-menu__column">
                            <h4>Catalog Pages<span class="sub-toggle"></span></h4>
                            <ul class="mega-menu__list">
                                <li><a href="shop-default.php">Shop Default</a>
                                </li>
                                <li><a href="shop-default.php">Shop Fullwidth</a>
                                </li>
                                <li><a href="shop-categories.php">Shop Categories</a>
                                </li>
                                <li><a href="shop-sidebar.php">Shop Sidebar</a>
                                </li>
                                <li><a href="shop-sidebar-without-banner.php">Shop Without Banner</a>
                                </li>
                                <li><a href="shop-carousel.php">Shop Carousel</a>
                                </li>
                            </ul>
                        </div>
                        <div class="mega-menu__column">
                            <h4>Product Layout<span class="sub-toggle"></span></h4>
                            <ul class="mega-menu__list">
                                <li><a href="product-default.php">Default</a>
                                </li>
                                <li><a href="product-extend.php">Extended</a>
                                </li>
                                <li><a href="product-full-content.php">Full Content</a>
                                </li>
                                <li><a href="product-box.php">Boxed</a>
                                </li>
                                <li><a href="product-sidebar.php">Sidebar</a>
                                </li>
                                <li><a href="product-default.php">Fullwidth</a>
                                </li>
                            </ul>
                        </div>
                        <div class="mega-menu__column">
                            <h4>Product Types<span class="sub-toggle"></span></h4>
                            <ul class="mega-menu__list">
                                <li><a href="product-default.php">Simple</a>
                                </li>
                                <li><a href="product-default.php">Color Swatches</a>
                                </li>
                                <li><a href="product-countdown.php">Countdown</a>
                                </li>
                                <li><a href="product-multi-vendor.php">Multi-Vendor</a>
                                </li>
                                <li><a href="product-instagram.php">Instagram</a>
                                </li>
                                <li><a href="product-affiliate.php">Affiliate</a>
                                </li>
                                <li><a href="product-on-sale.php">On sale</a>
                                </li>
                                <li><a href="product-video.php">Video Featured</a>
                                </li>
                                <li><a href="product-groupped.php">Grouped</a>
                                </li>
                                <li><a href="product-out-stock.php">Out Of Stock</a>
                                </li>
                            </ul>
                        </div>
                        <div class="mega-menu__column">
                            <h4>Woo Pages<span class="sub-toggle"></span></h4>
                            <ul class="mega-menu__list">
                                <li><a href="#">Shopping Cart</a>
                                </li>
                                <li><a href="checkout.php">Checkout</a>
                                </li>
                                <li><a href="#">Whishlist</a>
                                </li>
                                <li><a href="#">Compare</a>
                                </li>
                                <li><a href="#">Order Tracking</a>
                                </li>
                                <li><a href="my-account.php">My Account</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </li>
                <li class="menu-item-has-children has-mega-menu"><a href="#">Pages</a><span class="sub-toggle"></span>
                    <div class="mega-menu">
                        <div class="mega-menu__column">
                            <h4>Basic Page<span class="sub-toggle"></span></h4>
                            <ul class="mega-menu__list">
                                <li><a href="about-us.php">About Us</a>
                                </li>
                                <li><a href="#">Contact</a>
                                </li>
                                <li><a href="#">Faqs</a>
                                </li>
                                <li><a href="#">Comming Soon</a>
                                </li>
                                <li><a href="404-page.php">404 Page</a>
                                </li>
                            </ul>
                        </div>
                        <div class="mega-menu__column">
                            <h4>Vendor Pages<span class="sub-toggle"></span></h4>
                            <ul class="mega-menu__list">
                                <li><a href="#">Become a Vendor</a>
                                </li>
                                <li><a href="#">Vendor Store</a>
                                </li>
                                <li><a href="#">Vendor Dashboard Free</a>
                                </li>
                                <li><a href="#">Vendor Dashboard Pro</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </li>
                <li class="menu-item-has-children has-mega-menu"><a href="#">Blogs</a><span class="sub-toggle"></span>
                    <div class="mega-menu">
                        <div class="mega-menu__column">
                            <h4>Blog Layout<span class="sub-toggle"></span></h4>
                            <ul class="mega-menu__list">
                                <li><a href="#">Grid</a>
                                </li>
                                <li><a href="#">Listing</a>
                                </li>
                                <li><a href="#">Small Thumb</a>
                                </li>
                                <li><a href="#">Left Sidebar</a>
                                </li>
                                <li><a href="#">Right Sidebar</a>
                                </li>
                            </ul>
                        </div>
                        <div class="mega-menu__column">
                            <h4>Single Blog<span class="sub-toggle"></span></h4>
                            <ul class="mega-menu__list">
                                <li><a href="#">Single 1</a>
                                </li>
                                <li><a href="#">Single 2</a>
                                </li>
                                <li><a href="#">Single 3</a>
                                </li>
                                <li><a href="#">Single 4</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </div>
